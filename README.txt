CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* FAQ
* Maintainers

INTRODUCTION
------------

This module allows you to have sitewide Drupal page caching turned on,
but omit specific pages.

* For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/jimafisk/2525998

* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2525998

REQUIREMENTS
------------

This module does not require any modules

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.
 See: https://drupal.org/documentation/install/modules-themes/modules-7
 for further information.

CONFIGURATION
-------------

* Requires users to have "administer site configuration" permission
* Settings: Administration » Configuration » Development » Uncache

FAQ
---

Q: Why aren't pages immediately showing real-time updates after being Uncached?
A: You must wait for the specified lifetime to expire for already cached pages,
 or manually clear the cache.

MAINTAINERS
-----------

Current maintainers:
 * Jim Fisk (jimafisk) - https://www.drupal.org/u/jimafisk
